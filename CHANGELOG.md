# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-deployment/compare/v1.0.0...v1.1.0) (2023-04-03)


### Features

* Add async task config in dev ([891ba58](https://gitlab.com/ifb-elixirfr/fair/metark/metark-deployment/commit/891ba58faca55af8ffcd3e8318fa577e0a1c1ee7))

## 1.0.0 (2023-03-06)


### Features

* Create a repo to run Metark app ([4871fa1](https://gitlab.com/ifb-elixirfr/fair/metark/metark-deployment/commit/4871fa1f51cdddcdc834d2135f149732773f09b7))
